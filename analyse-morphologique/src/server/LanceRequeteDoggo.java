package server;

import ca.uqac.lif.jerrydog.CallbackResponse;
import ca.uqac.lif.jerrydog.RequestCallback;
import com.sun.net.httpserver.HttpExchange;
import grammaire.Reorganisateur;
import grammaire.Tal_sqlLexer;
import grammaire.Tal_sqlParser;
import org.antlr.runtime.ANTLRReaderStream;
import org.antlr.runtime.CommonTokenStream;
import requetage.Lexique;
import requetage.MainRequetes;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LanceRequeteDoggo extends RequestCallback
{
    String username;
    String password;
    String url;
    String requete = "";
    String nom;
    int nbre;
    HashMap<String, String> lexiqueExpressions;
    HashMap<String, String> lexiqueRemplacement;
    Lexique lexiqueCorrection;
    Pattern dateRegex = Pattern.compile("\\d{4}");
    Matcher matcher;
    int nb_results;
    ArrayList<String> termesDeRecherche;
    ///HashMap<String, ArrayList<String>> indexMots = IndexMot.genererIndex("indexTexte.txt");



    public LanceRequeteDoggo(){
        super();

        lexiqueExpressions = MainRequetes.genererLexique("lexiqueExpression.csv");
        lexiqueRemplacement = MainRequetes.genererLexique("lexiquePivot.csv");
        lexiqueCorrection = new Lexique("lemmes.txt");
    }


    @Override
    public boolean fire(HttpExchange httpExchange)
    {
        String path = httpExchange.getRequestURI().getPath();
        return path.compareTo("/rocket") == 0;
    }

    @Override
    public CallbackResponse process(HttpExchange httpExchange)
    {
        ArrayList<Object> requetes = new ArrayList<>();
        long startTime = System.nanoTime();
        termesDeRecherche =new ArrayList<>();
        nb_results = 0;

        CallbackResponse response = new CallbackResponse(httpExchange);
        response.setContentType("text/html");
        String resp = "";
        StringBuilder out = new StringBuilder();

        out.append("<html>");
        out.append("<head>");
        out.append("<meta charset=\"utf-8\" />");
        out.append("<title>Lance requete!</title>");
        out.append("<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">");
        out.append("</head>");
        out.append("<body>");

        //container
        out.append("<div class=\"container text-center\">");

        out.append("<img style=\"width:15%;\" src=\"https://image.flaticon.com/icons/svg/1006/1006648.svg\" />");
        out.append("<h1>Rocket !</h1>");
        out.append("<h2>Le moteur de recherche qui lance des requêtes</h2>");


        // ---- configure START
        username = "lo17xxx";
        password = "dblo17";
        // The URL that will connect to TECFA's MySQL server
        // Syntax: jdbc:TYPE:machine:port/DB_NAME
        url = "jdbc:postgresql://tuxa.sme.utc/dblo17";
        // dans certaines configurations locales il faut définir l'url par :
        // url = "jdbc:postgresql://tuxa.sme.utc
        // ---- configure END

        String requete;
        requete = getStringFromInputStream(httpExchange.getRequestBody());
        requetes.add(requete);
        requete = requete.replace("requete=", "");


        out.append("<form action = \"http://localhost:1235/rocket\" method=\"post\" enctype='text/plain'>\n" +
                "<div class=\"input-group mb-3\">\n" +
                "  <input type=\"text\" value=\""+requete+"\" name=requete class=\"form-control\" placeholder=\"Entrez votre requête\" >\n" +
                "  <div class=\"input-group-append\">\n" +
                "    <button class=\"btn btn-outline-secondary\" type=\"submit\" id=\"button-addon2\">\uD83D\uDE80</button>\n" +
                "  </div>\n" +
                "</div>\n"+
                "</form>");

        if(!requete.equals(""))
        {
            requete = requete.toLowerCase();
            requete = requete.replace("'", " ");
            requete = requete.replace("’", " ");
            requete = requete.replace(",", " ");
            requete = requete.replace(".", " ");
            //requete = requete.replace("\"", " ");
            requete = requete.replace("?", " ");
            requete = requete.replace("!", " ");
            requete = requete.replace(";", " ");
            requete = requete.replace("/", " ");
            requete = requete.replace("(", " ");
            requete = requete.replace(")", " ");
            requete = requete.replace("-", " ");

            System.out.println("requetage.Saisie\t:\t" + requete);


            // Ajout d'éléments au lexique
            Utils.ajouterElementsAuLexique(requete, lexiqueCorrection, out);
            requete = requete.replace("\"", " ");


            // Correction
            StringBuilder requeteCorrigee = new StringBuilder();
            for (String mot :
                    requete.split(" "))
            {

                matcher = dateRegex.matcher(mot);
                // si le mot est à supprimer plus tard, on ne le touche pas
                if (lexiqueRemplacement.containsKey(mot)|| matcher.find())
                {
                    requeteCorrigee.append(mot).append(" ");
                } else if (mot.length() == 1)
                {
                    // do nothing
                } else if (mot.length() < 3)
                {
                    requeteCorrigee.append(mot).append(" ");
                } else
                {
                    //FIXME prendre le meilleur mot !
                    ArrayList<String> motsPossibles = lexiqueCorrection.analyse(mot);
                    for (String s :
                            motsPossibles)
                    {
                        System.out.println(mot + " : " + s);
                        // On ajoute les mots possibles en les séparant de OU
                        requeteCorrigee.append(s).append(" ")
                                .append((!s.equals(motsPossibles.get(motsPossibles.size()-1))?"ou ":""));
                    }
                }
            }

            requetes.add(requeteCorrigee.toString());

            if (!(requete + " ").equals(requeteCorrigee.toString()))
            {
                out.append("<div class=\"alert alert-warning\" role=\"alert\"><h3>Nous avons corrigé votre requête : </h3>" + requeteCorrigee.toString() + "<br><i>(Pour forcer un mot, le mettre entre guillemets)</i></div>");
            }


            System.out.println("Corri.\t:\t" + requeteCorrigee.toString());

            // On remplace les expressions par le mot clé correspondant
            for (String expr :
                    lexiqueExpressions.keySet())
            {
                requete = requeteCorrigee.toString().replace(expr, lexiqueExpressions.get(expr));
            }

            StringBuilder requeteAvantReorg = new StringBuilder();
            // On essaie de remplacer les mots clés par les mots correspondants
            for (String s :
                    requete.split(" "))
            {
                if (!(("$").equals(lexiqueRemplacement.get(s))))
                {
                    if (lexiqueRemplacement.get(s) == null)
                    {
                        requeteAvantReorg.append(s + " ");
                    } else requeteAvantReorg.append(lexiqueRemplacement.get(s) + " ");
                }
            }

            requetes.add(requeteAvantReorg);

            System.out.println("Modif.\t:\t" + requeteAvantReorg.toString());


            // On essaie de remettre les mots dans l'ordre
            String requeteReorganisee = Reorganisateur.reorganiserRequete(requeteAvantReorg.toString());
            // extraction des termes de recherche
//            termesDeRecherche.addAll(Arrays.asList(requeteReorganisee.split("contient")[1].split(" ")));
//            termesDeRecherche = new ArrayList<String>(new LinkedHashSet<String>(termesDeRecherche));
//            termesDeRecherche.remove("ou");
//            termesDeRecherche.remove("et");
//            termesDeRecherche.remove(" ");
//            termesDeRecherche.remove("");

            System.out.println("Reorg.\t:\t" + requeteReorganisee);
            requetes.add(requeteReorganisee);


            // On enlève tous les espaces en trop
            String requeteFinale = requeteReorganisee.toString().trim().replaceAll(" +", " ");
            requetes.add(requeteFinale);
            System.out.println("Finale\t:\t" + requeteFinale.toString());


            String arbre = "";
            // On affiche la requête sous forme SQL
            try
            {
                Tal_sqlLexer lexer = new Tal_sqlLexer(new ANTLRReaderStream(new StringReader(requeteFinale.toString())));
                CommonTokenStream tokens = new CommonTokenStream(lexer);
                Tal_sqlParser parser = new Tal_sqlParser(tokens);
                arbre = parser.listerequetes();
                System.out.println("Arbre\t:\t" + arbre);
                out.append("<h3>Notre petit moteur a généré la requête SQL suivante : </h3><pre>" + arbre + "</pre><h2>Résultats</h2><h3><strong>$results$</strong> résultats en <strong>$time$</strong> secondes</h3>");

            } catch (Exception e)
            {
                e.printStackTrace();
            }


            System.out.println(arbre);
            requetes.add(arbre);

            // INSTALL/load the Driver (Vendor specific Code)
            try
            {
                Class.forName("org.postgresql.Driver");
            } catch (java.lang.ClassNotFoundException e)
            {
                System.err.print("ClassNotFoundException: ");
                System.err.println(e.getMessage());
            }
            try
            {
                Connection con;
                Statement stmt;
                // Establish Connection to the database at URL with usename and password
                con = DriverManager.getConnection(url, username, password);
                stmt = con.createStatement();
                // Send the query and bind to the result set
                ResultSet rs = stmt.executeQuery(arbre);
                ResultSetMetaData rsmd = rs.getMetaData();
                nbre = rsmd.getColumnCount();
                out.append("<table class=\"table table-hover table-striped\"><thead>\n" +
                        "    <tr>\n" +
                        "      <th scope=\"col\">Fichier</th>\n" +
                        "      <th scope=\"col\">Titre</th>\n" );

                // On ajoute les colonnes qui correspondent au termes de recherche
//                for (String t :
//                        this.termesDeRecherche)
//                {
//                    out.append("<th scope=\"col\">"+t+"</th>");
//                }

                out.append(
                        "      <th scope=\"col\">Date</th>\n"+" </tr>\n" +
                        "  </thead><tbody>");


                while (rs.next())
                {
                    nb_results++;
                    for (int i = 1; i <= nbre; i++)
                    {
                        nom = rsmd.getColumnName(i);
                        String s = rs.getString(nom);
                        if (nom.equals("fichier"))
                        {
                            out.append("<tr><td><a href=\"files/").append(s).append("\" >").append(s).append("</a></td>");
                            out.append("<td>"+Utils.extractTitle(s)+"</td>");

//                            System.out.println(this.termesDeRecherche.size());
//                            for (String t :
//                                    this.termesDeRecherche)
//                            {
//                                System.out.println(this.indexMots.get(t));
//                                out.append("<td class=\"");
//                                if (this.indexMots.get(t) == null)
//                                {
//                                    out.append("bg-danger");
//                                }else if(this.indexMots.get(t).contains(s.toLowerCase())){
//                                    out.append("bg-success");
//                                } else {
//                                    out.append("bg-danger");
//                                }
//                                out.append("\"></td>");
//                            }
                            
                        } else if (nom.equals("jour"))
                        {
                            out.append("<td>"+s);
                        }else if (nom.equals("mois"))
                        {
                            out.append("/"+s);
                        }                
                        else if (nom.equals("annee"))
                        {
                            out.append("/"+s+"</td></tr>");
                        }
                        else
                        {
                            out.append("<h1>" + s + "</h1>");
                        }

                    }

                }
                out.append("</tbody></table>");
                //container
                out.append("<h1>Traitements</h1>");
                out.append("<table class=\"table table-hover\">\n" +
                        "  <tr>\n" +
                        "    <td class=\"tg-s268\">Saisie<br></td>\n" +
                        "    <td class=\"tg-0lax\">"+requetes.get(0)+"</td>\n" +
                        "  </tr>\n" +
                        "  <tr>\n" +
                        "    <td class=\"tg-0lax\">Correction</td>\n" +
                        "    <td class=\"tg-0lax\">"+requetes.get(1)+"</td>\n" +
                        "  </tr>\n" +
                        "  <tr>\n" +
                        "    <td class=\"tg-0lax\">Modification</td>\n" +
                        "    <td class=\"tg-0lax\">"+requetes.get(2)+"</td>\n" +
                        "  </tr>\n" +
                        "  <tr>\n" +
                        "    <td class=\"tg-0lax\">Reorganisation</td>\n" +
                        "    <td class=\"tg-0lax\">"+requetes.get(3)+"</td>\n" +
                        "  </tr>\n" +
                        "  <tr>\n" +
                        "    <td class=\"tg-0lax\">Presque-SQL<br></td>\n" +
                        "    <td class=\"tg-0lax\">"+requetes.get(4)+"</td>\n" +
                        "  </tr>\n" +
                        "  <tr>\n" +
                        "    <td class=\"tg-0lax\">SQL</td>\n" +
                        "    <td class=\"tg-0lax\">"+requetes.get(5)+"</td>\n" +
                        "  </tr>\n" +
                        "</table>");
                out.append("</div>");
                out.append("</body>");
                out.append("</html>");
                // Close resources
                stmt.close();
                con.close();
            }
            // print out decent erreur messages
            catch (SQLException ex)
            {
                System.err.println("==> SQLException: ");
                while (ex != null)
                {
                    System.out.println("Message:   " + ex.getMessage());
                    System.out.println("SQLState:  " + ex.getSQLState());
                    System.out.println("ErrorCode: " + ex.getErrorCode());
                    ex = ex.getNextException();

                    out.append("<div class=\"alert alert-danger\" role=\"alert\"><h1>La requête n'est pas valide \uD83D\uDE1E </h1></div>");
                    //container
                    out.append("</div>");
                    out.append("</body>");
                    out.append("</html>");

                }
            }

        }

        long endTime   = System.nanoTime();
        Double totalTime = (endTime - startTime) * 0.000000001;
//        System.out.println(totalTime);
        Utils.replaceSubString(out, "$time$", totalTime.toString());
        Utils.replaceSubString(out, "$results$", Integer.toString(nb_results));

        response.setContents(out.toString());

        return response;
    }




    public static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();
    }
}
