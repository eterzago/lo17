package server;

import requetage.Lexique;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils
{
    public static String getFileContent(String fileName){
        byte[] encoded = new byte[0];
        try
        {
            encoded = Files.readAllBytes(Paths.get("Fichiers/pages/"+fileName));
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return new String(encoded, Charset.forName("UTF-8"));
    }

    public static String extractTitle(String fileName){
        String fileContent = Utils.getFileContent(fileName.replace(" ",""));


        Pattern titrePattern = Pattern.compile(".*<title>(.*)</title>.*");
        Matcher titre =  titrePattern.matcher(fileContent);
        titre.find();

        return titre.group(1).split("&gt;")[2];
    }

    public static boolean fileContains(String fileName, String word){
        String fileContent = Utils.getFileContent(fileName.replace(" ",""));
        return fileContent.toLowerCase().contains(word.toLowerCase());
    }

    public static void replaceSubString(StringBuilder builder, String from, String to)
    {
        System.out.println("Replacing "+from+" by "+to);
        int index = builder.indexOf(from);
        while (index != -1)
        {
            System.out.println("loop");
            builder.replace(index, index + from.length(), to);
            index += to.length(); // Move to the end of the replacement
            index = builder.indexOf(from, index);
        }
    }


    public static void ajouterElementsAuLexique(String requete, Lexique lexique, StringBuilder out){
        Pattern titrePattern = Pattern.compile(".*\"(.*)\".*");
        Matcher titre =  titrePattern.matcher(requete);
        if(titre.find()){
            lexique.ajouterMotAuLexique(titre.group(1));
            out.append("<div class=\"alert alert-success\" role=\"alert\">\n" +
                    "  Mot <strong>"+titre.group(1)+"</strong> ajouté au Lexique\n" +
                    "</div>");
            System.out.println("Ajout du mot au lexique : "+titre.group(1));
        }

    }



//    public static boolean containsIgnoreCase(String str, String searchStr)     {
//        if(str == null || searchStr == null) return false;
//
//        final int length = searchStr.length();
//        if (length == 0)
//            return true;
//
//        for (int i = str.length() - length; i >= 0; i--) {
//            if (str.regionMatches(true, i, searchStr, 0, length))
//                return true;
//        }
//        return false;
//    }
}


