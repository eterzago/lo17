package server;

import ca.uqac.lif.jerrydog.CallbackResponse;
import ca.uqac.lif.jerrydog.RequestCallback;
import com.sun.net.httpserver.HttpExchange;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GetFile extends RequestCallback
{

    @Override
    public boolean fire(HttpExchange httpExchange)
    {
        String path = httpExchange.getRequestURI().getPath();
        return path.startsWith("/file");

    }

    @Override
    public CallbackResponse process(HttpExchange httpExchange)
    {
        CallbackResponse cbr = new CallbackResponse(httpExchange);

        String parts[] = httpExchange.getRequestURI().getPath().split("/");
        String time = Utils.getFileContent(parts[2]);
        if (time == null) {
            cbr.setCode(CallbackResponse.HTTP_NOT_FOUND);
        } else {
            cbr.setCode(CallbackResponse.HTTP_OK).setContents(time);
        }
        return cbr;
    }



}
