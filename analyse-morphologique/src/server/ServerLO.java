package server;

import ca.uqac.lif.jerrydog.Server;

import java.io.IOException;

public class ServerLO
{
    public static void main(String args[])
    {
        Server s = new Server();
        s.setServerPort(1235);
        s.registerCallback(new LanceRequeteDoggo());
        s.registerCallback(new GetFile());

        try
        {
            System.out.println("Serveur lancé...");
            s.startServer();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
