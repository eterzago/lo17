package grammaire;

import java.util.ArrayList;
import java.util.Arrays;

public class Reorganisateur
{
    public static String reorganiserRequete(String requete){

        StringBuilder nouvelleRequete = new StringBuilder();
        ArrayList<String> requeteDecoupee = new ArrayList<>();
        requeteDecoupee.addAll(Arrays.asList(requete.split(" ")));

        // S'il y a un select
        if (requeteDecoupee.contains("vouloir")){
            nouvelleRequete.append("vouloir ");
            requeteDecoupee.remove("vouloir");
        } else {
            // Peut apporter des erreurs
            nouvelleRequete.append("vouloir ");
        }


        if(!(requeteDecoupee.contains("nombre")
                ||requeteDecoupee.contains("auteur")
                ||requeteDecoupee.contains("article")
                ||requeteDecoupee.contains("rubrique")
                ||requeteDecoupee.contains("bulletin")
        )){
            nouvelleRequete.append("article contient ");
        }
        else
        {
            // Choix de la table
            if (requeteDecoupee.contains("nombre"))
            {
                nouvelleRequete.append("nombre ");
                requeteDecoupee.remove("nombre");
            }
            if (requeteDecoupee.contains("auteur"))
            {
                nouvelleRequete.append("auteur ");
                requeteDecoupee.remove("auteur");
                requeteDecoupee.remove("article");
                requeteDecoupee.remove("rubrique");

            }
            if (requeteDecoupee.contains("article"))
            {
                nouvelleRequete.append("article ");
                requeteDecoupee.remove("article");
            }
            if (requeteDecoupee.contains("rubrique"))
            {
                nouvelleRequete.append("rubrique ");
                requeteDecoupee.remove("rubrique");
            }

            if (requeteDecoupee.contains("bulletin"))
            {
                nouvelleRequete.append("bulletin ");
                requeteDecoupee.remove("bulletin");
            }
        }




        // Where
        if (requeteDecoupee.contains("contient")){
            nouvelleRequete.append("contient ");
            while(requeteDecoupee.contains("contient")){
                requeteDecoupee.remove("contient");
            }
        }

        // ...

        if(nouvelleRequete.toString().contains("avant") || nouvelleRequete.toString().contains("après"))
        {
            System.out.println("DATE !");
        }

        for (String motRestant:
                requeteDecoupee) {
            nouvelleRequete.append(motRestant + " ");
        }

        return nouvelleRequete.toString();
    }
}
