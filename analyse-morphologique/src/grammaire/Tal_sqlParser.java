// $ANTLR 3.5.1 /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g 2019-01-06 19:13:56
package grammaire;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class Tal_sqlParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "ARTICLE", "AUTEUR", "COMPTE", 
		"CONTIENT", "DATE", "ETOU", "NOM", "RUBRIQUE", "VAR", "VOULOIR", "WS"
	};
	public static final int EOF=-1;
	public static final int ARTICLE=4;
	public static final int AUTEUR=5;
	public static final int COMPTE=6;
	public static final int CONTIENT=7;
	public static final int DATE=8;
	public static final int ETOU=9;
	public static final int NOM=10;
	public static final int RUBRIQUE=11;
	public static final int VAR=12;
	public static final int VOULOIR=13;
	public static final int WS=14;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public Tal_sqlParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public Tal_sqlParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return Tal_sqlParser.tokenNames; }
	@Override public String getGrammarFileName() { return "/home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g"; }



	// $ANTLR start "listerequetes"
	// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:34:1: listerequetes returns [String sql = \"\"] : r= requete ;
	public final String listerequetes() throws RecognitionException {
		String sql =  "";


		Arbre r =null;

		Arbre lr_arbre;
		try {
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:35:25: (r= requete )
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:36:3: r= requete
			{
			pushFollow(FOLLOW_requete_in_listerequetes203);
			r=requete();
			state._fsp--;


							lr_arbre = r;
							sql = lr_arbre.sortArbre();
						
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return sql;
	}
	// $ANTLR end "listerequetes"



	// $ANTLR start "requete"
	// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:43:1: requete returns [Arbre req_arbre = new Arbre(\"\")] : VOULOIR ( COMPTE ARTICLE | COMPTE AUTEUR | ARTICLE | AUTEUR | ARTICLE RUBRIQUE ) ( CONTIENT (ps= params_contient )+ | NOM ps= params_auteur ) ( DATE )? ;
	public final Arbre requete() throws RecognitionException {
		Arbre req_arbre =  new Arbre("");


		Arbre ps =null;

		Arbre ps_arbre; boolean firstParam= false;
		try {
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:44:53: ( VOULOIR ( COMPTE ARTICLE | COMPTE AUTEUR | ARTICLE | AUTEUR | ARTICLE RUBRIQUE ) ( CONTIENT (ps= params_contient )+ | NOM ps= params_auteur ) ( DATE )? )
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:45:3: VOULOIR ( COMPTE ARTICLE | COMPTE AUTEUR | ARTICLE | AUTEUR | ARTICLE RUBRIQUE ) ( CONTIENT (ps= params_contient )+ | NOM ps= params_auteur ) ( DATE )?
			{
			match(input,VOULOIR,FOLLOW_VOULOIR_in_requete230); 

							req_arbre.ajouteFils(new Arbre("","SELECT DISTINCT"));
						
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:49:3: ( COMPTE ARTICLE | COMPTE AUTEUR | ARTICLE | AUTEUR | ARTICLE RUBRIQUE )
			int alt1=5;
			switch ( input.LA(1) ) {
			case COMPTE:
				{
				int LA1_1 = input.LA(2);
				if ( (LA1_1==ARTICLE) ) {
					alt1=1;
				}
				else if ( (LA1_1==AUTEUR) ) {
					alt1=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 1, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case ARTICLE:
				{
				int LA1_2 = input.LA(2);
				if ( (LA1_2==RUBRIQUE) ) {
					alt1=5;
				}
				else if ( (LA1_2==CONTIENT||LA1_2==NOM) ) {
					alt1=3;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 1, 2, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case AUTEUR:
				{
				alt1=4;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}
			switch (alt1) {
				case 1 :
					// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:49:4: COMPTE ARTICLE
					{
					match(input,COMPTE,FOLLOW_COMPTE_in_requete242); 
					match(input,ARTICLE,FOLLOW_ARTICLE_in_requete244); 

								req_arbre.ajouteFils(new Arbre("","count(fichier)"));
								req_arbre.ajouteFils(new Arbre("","from public.titretexte"));
								
					}
					break;
				case 2 :
					// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:54:5: COMPTE AUTEUR
					{
					match(input,COMPTE,FOLLOW_COMPTE_in_requete255); 
					match(input,AUTEUR,FOLLOW_AUTEUR_in_requete257); 

								req_arbre.ajouteFils(new Arbre("","count(auteur)"));
								req_arbre.ajouteFils(new Arbre("","from public.email INNER JOIN public.titretexte ON public.email.fichier = public.titretexte.fichier"));
								
					}
					break;
				case 3 :
					// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:59:4: ARTICLE
					{
					match(input,ARTICLE,FOLLOW_ARTICLE_in_requete267); 

								req_arbre.ajouteFils(new Arbre("","public.titretexte.fichier, jour, mois, annee"));
								req_arbre.ajouteFils(new Arbre("","from public.titretexte INNER JOIN public.date ON public.date.fichier=public.titretexte.fichier"));
								
					}
					break;
				case 4 :
					// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:64:4: AUTEUR
					{
					match(input,AUTEUR,FOLLOW_AUTEUR_in_requete277); 

								req_arbre.ajouteFils(new Arbre("","email"));
								req_arbre.ajouteFils(new Arbre("","from public.email INNER JOIN public.titretexte ON public.email.fichier = public.titretexte.fichier"));
								
					}
					break;
				case 5 :
					// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:69:4: ARTICLE RUBRIQUE
					{
					match(input,ARTICLE,FOLLOW_ARTICLE_in_requete287); 
					match(input,RUBRIQUE,FOLLOW_RUBRIQUE_in_requete289); 

								req_arbre.ajouteFils(new Arbre("","public.rubrique.rubrique"));
								req_arbre.ajouteFils(new Arbre("","from public.rubrique inner join public.titretexte on public.titretexte.rubrique = public.rubrique.rubrique"));
								
					}
					break;

			}

			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:75:3: ( CONTIENT (ps= params_contient )+ | NOM ps= params_auteur )
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0==CONTIENT) ) {
				alt3=1;
			}
			else if ( (LA3_0==NOM) ) {
				alt3=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				throw nvae;
			}

			switch (alt3) {
				case 1 :
					// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:75:4: CONTIENT (ps= params_contient )+
					{
					match(input,CONTIENT,FOLLOW_CONTIENT_in_requete302); 

									
									if(!firstParam)
									{
										req_arbre.ajouteFils(new Arbre("", "where" ));
										firstParam = true;
									}
									else
									{
										req_arbre.ajouteFils(new Arbre("", "AND" ));	

									}
								
					// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:89:6: (ps= params_contient )+
					int cnt2=0;
					loop2:
					while (true) {
						int alt2=2;
						int LA2_0 = input.LA(1);
						if ( (LA2_0==VAR) ) {
							alt2=1;
						}

						switch (alt2) {
						case 1 :
							// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:89:6: ps= params_contient
							{
							pushFollow(FOLLOW_params_contient_in_requete315);
							ps=params_contient();
							state._fsp--;

							}
							break;

						default :
							if ( cnt2 >= 1 ) break loop2;
							EarlyExitException eee = new EarlyExitException(2, input);
							throw eee;
						}
						cnt2++;
					}


									ps_arbre = ps;
									req_arbre.ajouteFils(ps_arbre);
								
					}
					break;
				case 2 :
					// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:94:4: NOM ps= params_auteur
					{
					match(input,NOM,FOLLOW_NOM_in_requete327); 

									if(!firstParam)
									{
										req_arbre.ajouteFils(new Arbre("", "where" ));
										firstParam = true;
									}
									else
									{
										req_arbre.ajouteFils(new Arbre("", "AND" ));	
									}
								
					pushFollow(FOLLOW_params_auteur_in_requete340);
					ps=params_auteur();
					state._fsp--;


									ps_arbre = ps;
									req_arbre.ajouteFils(ps_arbre);
								
					}
					break;

			}

			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:111:4: ( DATE )?
			int alt4=2;
			int LA4_0 = input.LA(1);
			if ( (LA4_0==DATE) ) {
				alt4=1;
			}
			switch (alt4) {
				case 1 :
					// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:111:5: DATE
					{
					match(input,DATE,FOLLOW_DATE_in_requete352); 
					}
					break;

			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return req_arbre;
	}
	// $ANTLR end "requete"



	// $ANTLR start "params_contient"
	// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:114:1: params_contient returns [Arbre les_pars_arbre = new Arbre(\"\")] : par1= param_contient (etou_token= ETOU par2= param_contient )* ;
	public final Arbre params_contient() throws RecognitionException {
		Arbre les_pars_arbre =  new Arbre("");


		Token etou_token=null;
		Arbre par1 =null;
		Arbre par2 =null;

		Arbre par1_arbre, par2_arbre; 
		try {
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:115:41: (par1= param_contient (etou_token= ETOU par2= param_contient )* )
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:116:3: par1= param_contient (etou_token= ETOU par2= param_contient )*
			{
			pushFollow(FOLLOW_param_contient_in_params_contient380);
			par1=param_contient();
			state._fsp--;


							par1_arbre = par1;
							les_pars_arbre.ajouteFils(par1_arbre);
						
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:121:3: (etou_token= ETOU par2= param_contient )*
			loop5:
			while (true) {
				int alt5=2;
				int LA5_0 = input.LA(1);
				if ( (LA5_0==ETOU) ) {
					alt5=1;
				}

				switch (alt5) {
				case 1 :
					// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:121:4: etou_token= ETOU par2= param_contient
					{
					etou_token=(Token)match(input,ETOU,FOLLOW_ETOU_in_params_contient394); 
					pushFollow(FOLLOW_param_contient_in_params_contient400);
					par2=param_contient();
					state._fsp--;


								par2_arbre = par2;
								if(etou_token.getText().equals("et"))
								{
									les_pars_arbre.ajouteFils(new Arbre("", "AND"));
								}
								else if(etou_token.getText().equals("ou"))
								{
									les_pars_arbre.ajouteFils(new Arbre("", "OR"));
								}
								les_pars_arbre.ajouteFils(par2_arbre);

							
					}
					break;

				default :
					break loop5;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return les_pars_arbre;
	}
	// $ANTLR end "params_contient"



	// $ANTLR start "param_contient"
	// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:137:1: param_contient returns [Arbre lepar_arbre = new Arbre(\"\")] : a= VAR ;
	public final Arbre param_contient() throws RecognitionException {
		Arbre lepar_arbre =  new Arbre("");


		Token a=null;

		try {
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:137:60: (a= VAR )
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:138:2: a= VAR
			{
			a=(Token)match(input,VAR,FOLLOW_VAR_in_param_contient424); 
			 lepar_arbre.ajouteFils(new Arbre("mot =", "'"+a.getText()+"'"));
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return lepar_arbre;
	}
	// $ANTLR end "param_contient"



	// $ANTLR start "params_auteur"
	// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:142:1: params_auteur returns [Arbre les_pars_arbre = new Arbre(\"\")] : par1= param_auteur (etou_token= ETOU par2= param_contient )* ;
	public final Arbre params_auteur() throws RecognitionException {
		Arbre les_pars_arbre =  new Arbre("");


		Token etou_token=null;
		Arbre par1 =null;
		Arbre par2 =null;

		Arbre par1_arbre, par2_arbre;
		try {
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:143:40: (par1= param_auteur (etou_token= ETOU par2= param_contient )* )
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:144:3: par1= param_auteur (etou_token= ETOU par2= param_contient )*
			{
			pushFollow(FOLLOW_param_auteur_in_params_auteur454);
			par1=param_auteur();
			state._fsp--;


							par1_arbre = par1;
							les_pars_arbre.ajouteFils(par1_arbre);
						
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:149:3: (etou_token= ETOU par2= param_contient )*
			loop6:
			while (true) {
				int alt6=2;
				int LA6_0 = input.LA(1);
				if ( (LA6_0==ETOU) ) {
					alt6=1;
				}

				switch (alt6) {
				case 1 :
					// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:149:4: etou_token= ETOU par2= param_contient
					{
					etou_token=(Token)match(input,ETOU,FOLLOW_ETOU_in_params_auteur469); 
					pushFollow(FOLLOW_param_contient_in_params_auteur475);
					par2=param_contient();
					state._fsp--;


								par2_arbre = par2;
								if(etou_token.getText().equals("et"))
								{
									les_pars_arbre.ajouteFils(new Arbre("", "AND"));
								}
								else if(etou_token.getText().equals("ou"))
								{
									les_pars_arbre.ajouteFils(new Arbre("", "OR"));
								}
								les_pars_arbre.ajouteFils(par2_arbre);

							
					}
					break;

				default :
					break loop6;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return les_pars_arbre;
	}
	// $ANTLR end "params_auteur"



	// $ANTLR start "param_auteur"
	// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:165:1: param_auteur returns [Arbre lepar_arbre = new Arbre(\"\")] : a= VAR ;
	public final Arbre param_auteur() throws RecognitionException {
		Arbre lepar_arbre =  new Arbre("");


		Token a=null;

		try {
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:165:58: (a= VAR )
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:166:2: a= VAR
			{
			a=(Token)match(input,VAR,FOLLOW_VAR_in_param_auteur499); 
			 lepar_arbre.ajouteFils(new Arbre("auteur =", "'"+a.getText()+"'"));
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return lepar_arbre;
	}
	// $ANTLR end "param_auteur"

	// Delegated rules



	public static final BitSet FOLLOW_requete_in_listerequetes203 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VOULOIR_in_requete230 = new BitSet(new long[]{0x0000000000000070L});
	public static final BitSet FOLLOW_COMPTE_in_requete242 = new BitSet(new long[]{0x0000000000000010L});
	public static final BitSet FOLLOW_ARTICLE_in_requete244 = new BitSet(new long[]{0x0000000000000480L});
	public static final BitSet FOLLOW_COMPTE_in_requete255 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_AUTEUR_in_requete257 = new BitSet(new long[]{0x0000000000000480L});
	public static final BitSet FOLLOW_ARTICLE_in_requete267 = new BitSet(new long[]{0x0000000000000480L});
	public static final BitSet FOLLOW_AUTEUR_in_requete277 = new BitSet(new long[]{0x0000000000000480L});
	public static final BitSet FOLLOW_ARTICLE_in_requete287 = new BitSet(new long[]{0x0000000000000800L});
	public static final BitSet FOLLOW_RUBRIQUE_in_requete289 = new BitSet(new long[]{0x0000000000000480L});
	public static final BitSet FOLLOW_CONTIENT_in_requete302 = new BitSet(new long[]{0x0000000000001000L});
	public static final BitSet FOLLOW_params_contient_in_requete315 = new BitSet(new long[]{0x0000000000001102L});
	public static final BitSet FOLLOW_NOM_in_requete327 = new BitSet(new long[]{0x0000000000001000L});
	public static final BitSet FOLLOW_params_auteur_in_requete340 = new BitSet(new long[]{0x0000000000000102L});
	public static final BitSet FOLLOW_DATE_in_requete352 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_param_contient_in_params_contient380 = new BitSet(new long[]{0x0000000000000202L});
	public static final BitSet FOLLOW_ETOU_in_params_contient394 = new BitSet(new long[]{0x0000000000001000L});
	public static final BitSet FOLLOW_param_contient_in_params_contient400 = new BitSet(new long[]{0x0000000000000202L});
	public static final BitSet FOLLOW_VAR_in_param_contient424 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_param_auteur_in_params_auteur454 = new BitSet(new long[]{0x0000000000000202L});
	public static final BitSet FOLLOW_ETOU_in_params_auteur469 = new BitSet(new long[]{0x0000000000001000L});
	public static final BitSet FOLLOW_param_contient_in_params_auteur475 = new BitSet(new long[]{0x0000000000000202L});
	public static final BitSet FOLLOW_VAR_in_param_auteur499 = new BitSet(new long[]{0x0000000000000002L});
}
