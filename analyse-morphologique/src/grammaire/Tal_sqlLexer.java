// $ANTLR 3.5.1 /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g 2019-01-06 19:13:57

package grammaire;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class Tal_sqlLexer extends Lexer {
	public static final int EOF=-1;
	public static final int ARTICLE=4;
	public static final int AUTEUR=5;
	public static final int COMPTE=6;
	public static final int CONTIENT=7;
	public static final int DATE=8;
	public static final int ETOU=9;
	public static final int NOM=10;
	public static final int RUBRIQUE=11;
	public static final int VAR=12;
	public static final int VOULOIR=13;
	public static final int WS=14;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public Tal_sqlLexer() {} 
	public Tal_sqlLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public Tal_sqlLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "/home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g"; }

	// $ANTLR start "VOULOIR"
	public final void mVOULOIR() throws RecognitionException {
		try {
			int _type = VOULOIR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:3:9: ( 'vouloir' )
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:3:11: 'vouloir'
			{
			match("vouloir"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "VOULOIR"

	// $ANTLR start "COMPTE"
	public final void mCOMPTE() throws RecognitionException {
		try {
			int _type = COMPTE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:6:9: ( 'nombre' )
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:6:12: 'nombre'
			{
			match("nombre"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMPTE"

	// $ANTLR start "ARTICLE"
	public final void mARTICLE() throws RecognitionException {
		try {
			int _type = ARTICLE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:8:9: ( 'article' )
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:8:11: 'article'
			{
			match("article"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ARTICLE"

	// $ANTLR start "RUBRIQUE"
	public final void mRUBRIQUE() throws RecognitionException {
		try {
			int _type = RUBRIQUE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:11:9: ( 'rubrique' )
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:11:11: 'rubrique'
			{
			match("rubrique"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RUBRIQUE"

	// $ANTLR start "AUTEUR"
	public final void mAUTEUR() throws RecognitionException {
		try {
			int _type = AUTEUR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:14:8: ( 'auteur' )
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:14:10: 'auteur'
			{
			match("auteur"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AUTEUR"

	// $ANTLR start "ETOU"
	public final void mETOU() throws RecognitionException {
		try {
			int _type = ETOU;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:17:7: ( 'et' | 'ou' )
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0=='e') ) {
				alt1=1;
			}
			else if ( (LA1_0=='o') ) {
				alt1=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}

			switch (alt1) {
				case 1 :
					// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:17:10: 'et'
					{
					match("et"); 

					}
					break;
				case 2 :
					// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:17:17: 'ou'
					{
					match("ou"); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ETOU"

	// $ANTLR start "CONTIENT"
	public final void mCONTIENT() throws RecognitionException {
		try {
			int _type = CONTIENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:19:10: ( 'contient' )
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:19:12: 'contient'
			{
			match("contient"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CONTIENT"

	// $ANTLR start "NOM"
	public final void mNOM() throws RecognitionException {
		try {
			int _type = NOM;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:22:5: ( 'par' )
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:22:7: 'par'
			{
			match("par"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NOM"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:25:5: ( ( ' ' | '\\t' | '\\r' | 'je' | 'qui' | 'dont' ) | '\\n' )
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0=='\t'||LA3_0=='\r'||LA3_0==' '||LA3_0=='d'||LA3_0=='j'||LA3_0=='q') ) {
				alt3=1;
			}
			else if ( (LA3_0=='\n') ) {
				alt3=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				throw nvae;
			}

			switch (alt3) {
				case 1 :
					// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:25:7: ( ' ' | '\\t' | '\\r' | 'je' | 'qui' | 'dont' )
					{
					// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:25:7: ( ' ' | '\\t' | '\\r' | 'je' | 'qui' | 'dont' )
					int alt2=6;
					switch ( input.LA(1) ) {
					case ' ':
						{
						alt2=1;
						}
						break;
					case '\t':
						{
						alt2=2;
						}
						break;
					case '\r':
						{
						alt2=3;
						}
						break;
					case 'j':
						{
						alt2=4;
						}
						break;
					case 'q':
						{
						alt2=5;
						}
						break;
					case 'd':
						{
						alt2=6;
						}
						break;
					default:
						NoViableAltException nvae =
							new NoViableAltException("", 2, 0, input);
						throw nvae;
					}
					switch (alt2) {
						case 1 :
							// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:25:8: ' '
							{
							match(' '); 
							}
							break;
						case 2 :
							// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:25:13: '\\t'
							{
							match('\t'); 
							}
							break;
						case 3 :
							// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:25:20: '\\r'
							{
							match('\r'); 
							}
							break;
						case 4 :
							// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:25:27: 'je'
							{
							match("je"); 

							}
							break;
						case 5 :
							// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:25:34: 'qui'
							{
							match("qui"); 

							}
							break;
						case 6 :
							// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:25:42: 'dont'
							{
							match("dont"); 

							}
							break;

					}

					skip();
					}
					break;
				case 2 :
					// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:25:62: '\\n'
					{
					match('\n'); 
					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	// $ANTLR start "VAR"
	public final void mVAR() throws RecognitionException {
		try {
			int _type = VAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:29:6: ( ( ( 'A' .. 'Z' | 'a' .. 'z' | '\\u0080' .. '\\ufffe' ) ( 'a' .. 'z' | '\\u0080' .. '\\ufffe' )+ | ( '0' .. '9' )+ ) )
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:29:8: ( ( 'A' .. 'Z' | 'a' .. 'z' | '\\u0080' .. '\\ufffe' ) ( 'a' .. 'z' | '\\u0080' .. '\\ufffe' )+ | ( '0' .. '9' )+ )
			{
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:29:8: ( ( 'A' .. 'Z' | 'a' .. 'z' | '\\u0080' .. '\\ufffe' ) ( 'a' .. 'z' | '\\u0080' .. '\\ufffe' )+ | ( '0' .. '9' )+ )
			int alt6=2;
			int LA6_0 = input.LA(1);
			if ( ((LA6_0 >= 'A' && LA6_0 <= 'Z')||(LA6_0 >= 'a' && LA6_0 <= 'z')||(LA6_0 >= '\u0080' && LA6_0 <= '\uFFFE')) ) {
				alt6=1;
			}
			else if ( ((LA6_0 >= '0' && LA6_0 <= '9')) ) {
				alt6=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 6, 0, input);
				throw nvae;
			}

			switch (alt6) {
				case 1 :
					// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:29:9: ( 'A' .. 'Z' | 'a' .. 'z' | '\\u0080' .. '\\ufffe' ) ( 'a' .. 'z' | '\\u0080' .. '\\ufffe' )+
					{
					if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z')||(input.LA(1) >= '\u0080' && input.LA(1) <= '\uFFFE') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:29:50: ( 'a' .. 'z' | '\\u0080' .. '\\ufffe' )+
					int cnt4=0;
					loop4:
					while (true) {
						int alt4=2;
						int LA4_0 = input.LA(1);
						if ( ((LA4_0 >= 'a' && LA4_0 <= 'z')||(LA4_0 >= '\u0080' && LA4_0 <= '\uFFFE')) ) {
							alt4=1;
						}

						switch (alt4) {
						case 1 :
							// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:
							{
							if ( (input.LA(1) >= 'a' && input.LA(1) <= 'z')||(input.LA(1) >= '\u0080' && input.LA(1) <= '\uFFFE') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt4 >= 1 ) break loop4;
							EarlyExitException eee = new EarlyExitException(4, input);
							throw eee;
						}
						cnt4++;
					}

					}
					break;
				case 2 :
					// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:29:83: ( '0' .. '9' )+
					{
					// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:29:83: ( '0' .. '9' )+
					int cnt5=0;
					loop5:
					while (true) {
						int alt5=2;
						int LA5_0 = input.LA(1);
						if ( ((LA5_0 >= '0' && LA5_0 <= '9')) ) {
							alt5=1;
						}

						switch (alt5) {
						case 1 :
							// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt5 >= 1 ) break loop5;
							EarlyExitException eee = new EarlyExitException(5, input);
							throw eee;
						}
						cnt5++;
					}

					}
					break;

			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "VAR"

	// $ANTLR start "DATE"
	public final void mDATE() throws RecognitionException {
		try {
			int _type = DATE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:31:7: ( 'date ' VAR )
			// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:31:9: 'date ' VAR
			{
			match("date "); 

			mVAR(); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DATE"

	@Override
	public void mTokens() throws RecognitionException {
		// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:1:8: ( VOULOIR | COMPTE | ARTICLE | RUBRIQUE | AUTEUR | ETOU | CONTIENT | NOM | WS | VAR | DATE )
		int alt7=11;
		alt7 = dfa7.predict(input);
		switch (alt7) {
			case 1 :
				// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:1:10: VOULOIR
				{
				mVOULOIR(); 

				}
				break;
			case 2 :
				// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:1:18: COMPTE
				{
				mCOMPTE(); 

				}
				break;
			case 3 :
				// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:1:25: ARTICLE
				{
				mARTICLE(); 

				}
				break;
			case 4 :
				// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:1:33: RUBRIQUE
				{
				mRUBRIQUE(); 

				}
				break;
			case 5 :
				// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:1:42: AUTEUR
				{
				mAUTEUR(); 

				}
				break;
			case 6 :
				// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:1:49: ETOU
				{
				mETOU(); 

				}
				break;
			case 7 :
				// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:1:54: CONTIENT
				{
				mCONTIENT(); 

				}
				break;
			case 8 :
				// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:1:63: NOM
				{
				mNOM(); 

				}
				break;
			case 9 :
				// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:1:67: WS
				{
				mWS(); 

				}
				break;
			case 10 :
				// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:1:70: VAR
				{
				mVAR(); 

				}
				break;
			case 11 :
				// /home/paul/Documents/UTC/GI05/LO17/TD/TD4/Tal_sql.g:1:74: DATE
				{
				mDATE(); 

				}
				break;

		}
	}


	protected DFA7 dfa7 = new DFA7(this);
	static final String DFA7_eotS =
		"\16\uffff\5\15\2\40\2\15\1\11\10\15\1\uffff\1\15\1\54\1\11\10\15\1\uffff"+
		"\1\11\7\15\1\uffff\1\15\1\75\1\15\1\77\2\15\1\102\1\uffff\1\103\1\uffff"+
		"\2\15\2\uffff\1\106\1\107\2\uffff";
	static final String DFA7_eofS =
		"\110\uffff";
	static final String DFA7_minS =
		"\1\11\10\141\1\uffff\3\141\1\uffff\1\165\1\155\2\164\1\142\2\141\1\156"+
		"\1\162\1\141\1\151\1\156\1\164\1\154\1\142\1\151\1\145\1\162\1\uffff\1"+
		"\164\2\141\1\164\1\145\1\157\1\162\1\143\1\165\2\151\1\uffff\1\141\1\40"+
		"\1\151\1\145\1\154\1\162\1\161\1\145\1\uffff\1\162\1\141\1\145\1\141\1"+
		"\165\1\156\1\141\1\uffff\1\141\1\uffff\1\145\1\164\2\uffff\2\141\2\uffff";
	static final String DFA7_maxS =
		"\11\ufffe\1\uffff\3\ufffe\1\uffff\1\165\1\155\2\164\1\142\2\ufffe\1\156"+
		"\1\162\1\ufffe\1\151\1\156\1\164\1\154\1\142\1\151\1\145\1\162\1\uffff"+
		"\1\164\2\ufffe\1\164\1\145\1\157\1\162\1\143\1\165\2\151\1\uffff\1\ufffe"+
		"\1\40\1\151\1\145\1\154\1\162\1\161\1\145\1\uffff\1\162\1\ufffe\1\145"+
		"\1\ufffe\1\165\1\156\1\ufffe\1\uffff\1\ufffe\1\uffff\1\145\1\164\2\uffff"+
		"\2\ufffe\2\uffff";
	static final String DFA7_acceptS =
		"\11\uffff\1\11\3\uffff\1\12\22\uffff\1\6\13\uffff\1\10\10\uffff\1\13\7"+
		"\uffff\1\2\1\uffff\1\5\2\uffff\1\1\1\3\2\uffff\1\4\1\7";
	static final String DFA7_specialS =
		"\110\uffff}>";
	static final String[] DFA7_transitionS = {
			"\2\11\2\uffff\1\11\22\uffff\1\11\17\uffff\12\15\7\uffff\32\15\6\uffff"+
			"\1\3\1\15\1\7\1\14\1\5\4\15\1\12\3\15\1\2\1\6\1\10\1\13\1\4\3\15\1\1"+
			"\4\15\5\uffff\uff7f\15",
			"\16\15\1\16\13\15\5\uffff\uff7f\15",
			"\16\15\1\17\13\15\5\uffff\uff7f\15",
			"\21\15\1\20\2\15\1\21\5\15\5\uffff\uff7f\15",
			"\24\15\1\22\5\15\5\uffff\uff7f\15",
			"\23\15\1\23\6\15\5\uffff\uff7f\15",
			"\24\15\1\24\5\15\5\uffff\uff7f\15",
			"\16\15\1\25\13\15\5\uffff\uff7f\15",
			"\1\26\31\15\5\uffff\uff7f\15",
			"",
			"\4\15\1\27\25\15\5\uffff\uff7f\15",
			"\24\15\1\30\5\15\5\uffff\uff7f\15",
			"\1\32\15\15\1\31\13\15\5\uffff\uff7f\15",
			"",
			"\1\33",
			"\1\34",
			"\1\35",
			"\1\36",
			"\1\37",
			"\32\15\5\uffff\uff7f\15",
			"\32\15\5\uffff\uff7f\15",
			"\1\41",
			"\1\42",
			"\32\15\5\uffff\uff7f\15",
			"\1\43",
			"\1\44",
			"\1\45",
			"\1\46",
			"\1\47",
			"\1\50",
			"\1\51",
			"\1\52",
			"",
			"\1\53",
			"\32\15\5\uffff\uff7f\15",
			"\32\15\5\uffff\uff7f\15",
			"\1\55",
			"\1\56",
			"\1\57",
			"\1\60",
			"\1\61",
			"\1\62",
			"\1\63",
			"\1\64",
			"",
			"\32\15\5\uffff\uff7f\15",
			"\1\65",
			"\1\66",
			"\1\67",
			"\1\70",
			"\1\71",
			"\1\72",
			"\1\73",
			"",
			"\1\74",
			"\32\15\5\uffff\uff7f\15",
			"\1\76",
			"\32\15\5\uffff\uff7f\15",
			"\1\100",
			"\1\101",
			"\32\15\5\uffff\uff7f\15",
			"",
			"\32\15\5\uffff\uff7f\15",
			"",
			"\1\104",
			"\1\105",
			"",
			"",
			"\32\15\5\uffff\uff7f\15",
			"\32\15\5\uffff\uff7f\15",
			"",
			""
	};

	static final short[] DFA7_eot = DFA.unpackEncodedString(DFA7_eotS);
	static final short[] DFA7_eof = DFA.unpackEncodedString(DFA7_eofS);
	static final char[] DFA7_min = DFA.unpackEncodedStringToUnsignedChars(DFA7_minS);
	static final char[] DFA7_max = DFA.unpackEncodedStringToUnsignedChars(DFA7_maxS);
	static final short[] DFA7_accept = DFA.unpackEncodedString(DFA7_acceptS);
	static final short[] DFA7_special = DFA.unpackEncodedString(DFA7_specialS);
	static final short[][] DFA7_transition;

	static {
		int numStates = DFA7_transitionS.length;
		DFA7_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA7_transition[i] = DFA.unpackEncodedString(DFA7_transitionS[i]);
		}
	}

	protected class DFA7 extends DFA {

		public DFA7(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 7;
			this.eot = DFA7_eot;
			this.eof = DFA7_eof;
			this.min = DFA7_min;
			this.max = DFA7_max;
			this.accept = DFA7_accept;
			this.special = DFA7_special;
			this.transition = DFA7_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( VOULOIR | COMPTE | ARTICLE | RUBRIQUE | AUTEUR | ETOU | CONTIENT | NOM | WS | VAR | DATE );";
		}
	}

}
