package requetage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

// Préfixe seuil fixe
// Leven seuil dynamique en fonction de  la longueur du mot


public class Lexique {
    private HashMap<String, String> lemmes;
    public static final int SEUIL_MIN = 2;
    public static final int SEUIL_MAX = 5;
    public static final int SEUIL_ACCEPTATION = 100;

    public static final int SEUIL_LEVENSHTEIN = 10;

    public static final int COUT_SUPPR = 1;
    public static final int COUT_REMPL = 1;
    public static final int COUT_AJOUT = 1;

    private HashMap<Character,List<Character>> voisinsAZERTY;

    // Lit le fichier externe et le met dans une map
    public Lexique (String fichier)
    {
        voisinsAZERTY = new HashMap<>();
        ArrayList<Character> voisins = new ArrayList<>();

        // a
        voisins.add('z');
        voisins.add('s');
        voisins.add('q');
        voisinsAZERTY.put('a', voisins);
        voisins.clear();
        //z
        voisins.add('a');
        voisins.add('e');
        voisins.add('q');
        voisins.add('s');
        voisins.add('d');
        voisinsAZERTY.put('z',voisins);
        //...

        this.lemmes = new HashMap<>();
        try
        {
            FileReader reader = new FileReader("Fichiers/"+fichier);
            BufferedReader bufferedReader = new BufferedReader(reader);

            String line;

            while ((line = bufferedReader.readLine()) != null)
            {
                String[] splitLine = line.split(" \t");
                this.lemmes.put(splitLine[0],splitLine[1]);
            }
            reader.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Analyse un mot par rapport au corpus de mots connus
     * @param s mot à analyser
     * @return mots possibles
     */
    public ArrayList<String> analyse(String s)
    {
        // (a)
        s = s.toLowerCase();

        // (b)
        if(this.lemmes.get(s) != null)
        {
            ArrayList<String> a = new ArrayList<String>();
            a.add(this.lemmes.get(s));
            System.out.println("[CORRECTION] Lemmes trouvés : "+a.toString());
            return a;
        }
        // (c)
        else
        {
            ArrayList results = new ArrayList();
            float seuilActuel = SEUIL_ACCEPTATION;
            float prox;
            int index = 0;
            for (String mot : this.lemmes.keySet()) {
                prox = prefixeCommun(mot, s);
                if((prox > SEUIL_ACCEPTATION) && (prox >= seuilActuel))
                {
                    if (prox == seuilActuel)
                    {
                        index++;
                        results.add(index, this.lemmes.get(mot));
                    }
                    else
                    {
                        index = 0;
                        results.clear();
                        results.add(index, this.lemmes.get(mot));
                        seuilActuel = prox;
                    }
                }
            }
            if (!results.isEmpty())
            {
                return results;
            }
            // (d)
            else
            {
                ArrayList<String> resultsLeven = new ArrayList<>();
                float seuil = ((float)s.length())*0.19f; // TODO
                // probabilité de se tromper de touche
                // selon catégorie du mot, seuils
                // empirique + exemples dans le rapport
                // cout de remplacement dynamique en fonction de la distance au clavier
                // MAP lettre -> liste des voisins
                for (String mot : lemmes.keySet())
                {
                    int leven = distanceLevenshtein(s,mot);
                    if(leven <= seuil)
                    {
                        resultsLeven.add(mot);
                    }
                }
                if(!resultsLeven.isEmpty())
                {
                    return resultsLeven;
                }
                // (e)
                else
                {
                    //System.out.println("Aucun mot trouvé !");
                    ArrayList<String> retour = new ArrayList<String>();
                    retour.add(s);
                    return retour;
                }
            }
        }
    }

    /**
     * Calcule le pourcentage de similarité entre deux mots
     * en se basant sur le préfixe commun
     * @param mot1 premier mot
     * @param mot2 second mot
     * @return pourcentage de similarité
     */
    public float prefixeCommun(String mot1, String mot2)
    {
        int l1 = mot1.length();
        int l2 = mot2.length();
        if(l1 < SEUIL_MIN || l2 < SEUIL_MIN)
        {
            return 0;
        }
        else if(Math.abs(l1 - l2) > SEUIL_MAX)
        {
            return 0;
        }
        else
        {
            int i = 0;
            while (i < Math.min(l1,l2) && mot1.charAt(i) == mot2.charAt(i))
            {
                i++;
            }
            return (((float)i)/((float)Math.max(l1,l2))) * 100f;
        }
    }

    /**
     * Calcule la distance de Levenshtein
     * @param mot1 premier mot
     * @param mot2 second mot
     * @return distance de Levenshtein
     */
    public int distanceLevenshtein(String mot1, String mot2)
    {
        int l1 = mot1.length();
        int l2 = mot2.length();
        int[][] dist = new int[l1+1][l2+1];

        dist[0][0] = 0;
        for (int i=1; i <= l1; i++)
        {
            dist[i][0] = dist[i-1][0] + COUT_SUPPR;
        }
        for (int j=1; j <= l2; j++)
        {
            dist[0][j] = dist[0][j-1] + COUT_AJOUT;
        }
        for (int i=1; i <= l1; i++)
        {
            for (int j=1; j <= l2; j++)
            {
                int d1 = dist[i-1][j-1] + coutRempl(mot1.charAt(i-1),mot2.charAt(j-1));
                int d2 = dist[i-1][j] + COUT_SUPPR;
                int d3 = dist[i][j-1] + COUT_AJOUT;
                dist[i][j] = Math.min(Math.min(d1,d2),d3);
            }
        }
        //printMatrix(dist);
        return dist[l1-1][l2-1];
    }

    /**
     * Affiche une matrice dans la console
     * @param m matrice à afficher
     */
    public void printMatrix(int[][] m)
    {
        for (int i=0; i < m.length; i++)
        {
            for (int j=0; j < m[0].length; j++)
            {
                System.out.print(m[i][j]+" ");
            }
            System.out.print("\n");
        }
    }

    private int coutRempl(char a, char b)
    {
        // TODO cout dynamique en fonction de clavier AZERTY
        if(a == b) return 0;
        else return COUT_REMPL;
    }

    public void ajouterMotAuLexique(String mot){
        this.lemmes.put(mot, mot);
    }
}
