package requetage;

import java.util.Scanner;

public class Main {

    public static void main (String[] args)
    {
        Lexique lex = new Lexique("lemmes.txt");

        /*System.out.println(lex.prefixeCommun("chat","chaton"));
        System.out.println(lex.prefixeCommun("chat","grat"));
        System.out.println(lex.prefixeCommun("chat","chat"));
        System.out.println(lex.prefixeCommun("chat","carotte"));
*/

        Scanner keyb = new Scanner(System.in);
        System.out.print("Recherche : ");
        String recherche = keyb.nextLine();

        String[] rechercheSplit = recherche.split(" ");
        for (String mot :
             rechercheSplit) {
            //System.out.println(mot);
            //System.out.print("[");
            for (String s:
                 lex.analyse(mot))
            {
                System.out.print(s+" "); // FIXME pour "paste" ça trouve des trucs bizarres...
            }
            //System.out.print("] ");
        }

        /*requetage.Lexique lex = new requetage.Lexique();
        System.out.println("chat/chaton: "+lex.distanceLevenshtein("chat","chaton"));
        System.out.println("donald/trump: "+lex.distanceLevenshtein("donald","trump"));
        System.out.println("decoder/dcode: "+lex.distanceLevenshtein("decoder","dcode"));
        System.out.println("oslo/snow: "+lex.distanceLevenshtein("oslo","snow"));*/
    }
}
