package requetage;

import org.antlr.runtime.ANTLRReaderStream;
import org.antlr.runtime.CommonTokenStream;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;

import grammaire.Tal_sqlLexer;
import grammaire.Tal_sqlParser;

public class MainRequetes
{
    public static void main(String args[]){
        HashMap<String, String> lexiqueExpressions;
        lexiqueExpressions = MainRequetes.genererLexique("lexiqueExpression.csv");

        HashMap<String, String> lexiqueRemplacement;
        lexiqueRemplacement = MainRequetes.genererLexique("lexiquePivot.csv");

        Lexique lexiqueCorrection = new Lexique("lemmes.txt");

//        requetage.Lexique lexiqueFrancais = new requetage.Lexique("francais.txt");



        // requetage.Saisie de la requete
        Scanner keyb = new Scanner(System.in);
        System.out.print("Requete : ");
        String requete = keyb.nextLine();
        requete = requete.toLowerCase();
        requete = requete.replace("'", " ");
        requete = requete.replace("’", " ");
        requete = requete.replace(",", " ");
        requete = requete.replace(".", " ");
        requete = requete.replace("\"", " ");
        requete = requete.replace("?", " ");
        requete = requete.replace("!", " ");
        requete = requete.replace(";", " ");
        requete = requete.replace("/", " ");
        requete = requete.replace("(", " ");
        requete = requete.replace(")", " ");
        requete = requete.replace("-", " ");

        System.out.println("requetage.Saisie\t:\t"+requete);


        // Correction
        StringBuilder requeteCorrigee = new StringBuilder();
        for (String mot :
                requete.split(" ")) {
            // si le mot est à supprimer plus tard, on ne le touche pas
            if (lexiqueRemplacement.containsKey(mot)) {
                requeteCorrigee.append(mot).append(" ");
            }
            else if (mot.length()==1){
                // do nothing
            }
            else if(mot.length()<3){
                requeteCorrigee.append(mot).append(" ");
            }
            else {
                for (String s :
                        lexiqueCorrection.analyse(mot)) {
                    requeteCorrigee.append(s).append(" ");
                }
            }
        }

        System.out.println("Corri.\t:\t"+requeteCorrigee.toString());

        // On remplace les expressions par le mot clé correspondant
        for (String expr:
        lexiqueExpressions.keySet())
        {
            requete = requeteCorrigee.toString().replace(expr, lexiqueExpressions.get(expr));
        }

        StringBuilder requeteAvantReorg = new StringBuilder();
        // On essaie de remplacer les mots clés par les mots correspondants
        for (String s :
                requete.split(" "))
        {
            if(!(("$").equals(lexiqueRemplacement.get(s))))
            {
                if (lexiqueRemplacement.get(s) == null){
                    requeteAvantReorg.append(s + " ");
                }
                else requeteAvantReorg.append(lexiqueRemplacement.get(s)+" ");
            }
        }

        System.out.println("Modif.\t:\t"+requeteAvantReorg.toString());


        // On essaie de remettre les mots dans l'ordre
        String requeteReorganisee = reorganiserRequete(requeteAvantReorg.toString());
        System.out.println("Reorg.\t:\t"+requeteReorganisee);

        // On enlève tous les espaces en trop
        String requeteFinale  = requeteReorganisee.toString().trim().replaceAll(" +", " ");
        System.out.println("Finale\t:\t"+requeteFinale.toString());

        // On affiche la requête sous forme SQL
        try {
            Tal_sqlLexer lexer  = new Tal_sqlLexer(new ANTLRReaderStream(new StringReader(requeteFinale.toString())));
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            Tal_sqlParser parser = new Tal_sqlParser(tokens);
            String arbre = parser.listerequetes();
            System.out.println("Arbre\t:\t"+arbre);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public static HashMap<String, String> genererLexique(String fichier){

        HashMap<String, String> tmp = new HashMap<>();

        try
        {
            FileReader reader = new FileReader("Fichiers/"+fichier);
            BufferedReader bufferedReader = new BufferedReader(reader);

            String line;

            while ((line = bufferedReader.readLine()) != null)
            {
                //System.out.println(line);
                String[] splitLine = line.split(";");
                tmp.put(splitLine[0],splitLine[1]);
            }
            reader.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return tmp;
    }

    public static String reorganiserRequete(String requete){

        StringBuilder nouvelleRequete = new StringBuilder();
        ArrayList<String> requeteDecoupee = new ArrayList<>();
        requeteDecoupee.addAll(Arrays.asList(requete.split(" ")));

        // S'il y a un select
        if (requeteDecoupee.contains("vouloir")){
            nouvelleRequete.append("vouloir ");
            requeteDecoupee.remove("vouloir");
        } else {
            // Peut apporter des erreurs
            nouvelleRequete.append("vouloir ");
        }


        if(!(requeteDecoupee.contains("nombre")
                ||requeteDecoupee.contains("auteur")
                ||requeteDecoupee.contains("article")
                ||requeteDecoupee.contains("rubrique")
                ||requeteDecoupee.contains("bulletin")
            )){
            nouvelleRequete.append("article contient ");
        }
        else
        {
            // Choix de la table
            if (requeteDecoupee.contains("nombre"))
            {
                nouvelleRequete.append("nombre ");
                requeteDecoupee.remove("nombre");
            }
            if (requeteDecoupee.contains("auteur"))
            {
                nouvelleRequete.append("auteur ");
                requeteDecoupee.remove("auteur");
                requeteDecoupee.remove("article");
                requeteDecoupee.remove("rubrique");

            }
            if (requeteDecoupee.contains("article"))
            {
                nouvelleRequete.append("article ");
                requeteDecoupee.remove("article");
            }
            if (requeteDecoupee.contains("rubrique"))
            {
                nouvelleRequete.append("rubrique ");
                requeteDecoupee.remove("rubrique");
            }

            if (requeteDecoupee.contains("bulletin"))
            {
                nouvelleRequete.append("bulletin ");
                requeteDecoupee.remove("bulletin");
            }
        }




        // Where
        if (requeteDecoupee.contains("contient")){
            nouvelleRequete.append("contient ");
            while(requeteDecoupee.contains("contient")){
                requeteDecoupee.remove("contient");
            }
        }

        // ...


        for (String motRestant:
                requeteDecoupee) {
            nouvelleRequete.append(motRestant + " ");
        }

        return nouvelleRequete.toString();
    }

}
