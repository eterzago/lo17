package requetage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class IndexMot
{
    public static HashMap<String, ArrayList<String>> genererIndex(String fichier){

        HashMap<String, ArrayList<String>> tmp = new HashMap<>();

        try
        {
            FileReader reader = new FileReader("Fichiers/"+fichier);
            BufferedReader bufferedReader = new BufferedReader(reader);

            String line;

            while ((line = bufferedReader.readLine()) != null)
            {
                //System.out.println(line);
                String[] splitLine = line.split("\t");
                tmp.put(splitLine[0].toLowerCase(), new ArrayList<>());
                for (int i = 1; i <splitLine[1].length(); i++){
                    tmp.get(splitLine[0]).add(splitLine[i].toLowerCase());
                }
                System.out.println(splitLine[0]);
            }
            reader.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return tmp;
    }
}
